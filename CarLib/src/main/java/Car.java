public class Car {
    private boolean isRunning = false;
    public void TurnOnEngine() {
        isRunning = true;
    }
    public void TurnOffEngine() {
        isRunning = false;
    }
    public boolean GetState () {
        return isRunning;
    }
}
